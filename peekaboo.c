#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>
#include <time.h>
#include <dirent.h>
#include <unistd.h>

// CS students: Avert your eyes!!
// A global variable! We're all going to die!!!!!
int bytes_len = 0;

char* file_to_bytes(const char* filepath) {
    FILE* f = fopen(filepath, "rb");
    if (f == NULL) {
        fprintf(stderr, "There was an error opening file %s: %s! Aborting.", filepath, strerror(errno));
        exit(1);
    }
    fseek(f, 0, SEEK_END);
    long bytes = ftell(f);
    bytes_len = bytes;
    rewind(f);
    char* data = malloc(bytes * sizeof(char));
    if (fread(data, 1, bytes, f) != bytes) {
        fprintf(stderr, "There was an error reading file %s: %s! Aborting.", filepath, strerror(errno));
        exit(1);
    }
    fclose(f);
    return data;
}

int rand_range(int low, int high) {
    int diff = high - low;
    return (rand() % diff) + low;
}

char characters[] = "abcdefghijklmnopqrstuvwxyz.!";
int len = sizeof(characters) / sizeof(characters[0]);

void delete_file(const char* path) {
    if (remove(path) != 0) {
        fprintf(stderr, "There was an error removing file %s: %s! Aborting.", path, strerror(errno));
        exit(1);
    }
}

void write_file(const char* path, unsigned char* bytes) {
    FILE* f = fopen(path, "wb");
    if (f == NULL) {
        fprintf(stderr, "There was an error opening file %s: %s! Aborting.", path, strerror(errno));
        exit(1);
    }
    fwrite(bytes, 1, bytes_len, f);
    if (ferror(f)) {
        fprintf(stderr, "There was an error writing file %s: %s! Aborting.", path, strerror(errno));
        exit(1);
    }
}

void walk_dir(int levels_left) {
    if (levels_left == 0)
        return;

    DIR* dir = opendir(".");
    if (dir == NULL) {
        char* cwd = getcwd(NULL, 0);
        fprintf(stderr, "Couldn't open current directory %s: %s!", cwd, strerror(errno));
        exit(1);
    }
    struct dirent* current_file;
    int num_dirs = 0;
    while ((current_file = readdir(dir)) != NULL) {
        if (current_file->d_type == DT_DIR
            && current_file->d_name[0] != '.')
            ++num_dirs;
    }

    if (num_dirs == 0)
        return;

    int chosen_dir = rand_range(0, num_dirs);

    dir = opendir(".");
    if (dir == NULL) {
        char* cwd = getcwd(NULL, 0);
        fprintf(stderr, "Couldn't open current directory %s: %s!", cwd, strerror(errno));
        exit(1);
    }
    while ((current_file = readdir(dir)) != NULL) {
        if (current_file->d_type == DT_DIR
            && current_file->d_name[0] != '.') {

            if (chosen_dir-- == 0) {
                if (chdir(current_file->d_name) != 0) {
                    char* cwd = getcwd(NULL, 0);
                    fprintf(stderr, "Could not enter directory %s/%s: %s!", cwd, current_file->d_name, strerror(errno));
                    exit(1);
                }
                walk_dir(levels_left - 1);
            }
        }
    }
}


#define ROOT getenv("HOME")
#define FILE_NAME "peekaboo"
#define MAX_STEPS 3

int main(int argc, char** argv) {
    srand(time(0));

    if (argc != 1) {
        fprintf(stderr, "No arguments expected!");
        exit(1);
    }

    printf("\n\tYahaha! You found me!\n");

    unsigned char* bytes = (unsigned char*) file_to_bytes(argv[0]);

    delete_file(argv[0]);

    if (chdir(ROOT) != 0) {
        fprintf(stderr, "Could not enter directory %s: %s!", ROOT, strerror(errno));
        exit(1);
    }

    int steps = rand_range(1, MAX_STEPS);
    walk_dir(steps);

    char* cwd = getcwd(NULL, 0);

    write_file(FILE_NAME, bytes);
    free(bytes);

#ifndef _WIN32
    #include <sys/stat.h>
    if (chmod(FILE_NAME, 0755) != 0) {
        fprintf(stderr, "An error occurred trying to mark %s as executable: %s", FILE_NAME, strerror(errno));
        exit(1);
    }
#endif // _WIN32

    return 0;
}
