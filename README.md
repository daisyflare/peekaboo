# Peekaboo

A small peekaboo program, written in C.

Run it in your home directory, and it'll hide itself in a random file in your home directory!

## Getting started

``` sh
$ git clone https://www.gitlab.com/daisyflare/peekaboo
$ cd peekaboo
$ make
$ ./peekaboo
Yahaha! You found me!
```
Requires GNU make and a c compiler.
