.PHONY: all
all: peekaboo

peekaboo: peekaboo.c
	cc peekaboo.c -O3 -o peekaboo
